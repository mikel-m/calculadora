package xyz.yoandroide.calculadora;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalculadoraTest {

    private Calculadora calculadora;

    @Before
    public void setUp() {
        calculadora = new Calculadora();
    }

    @Test
    public void sumaTest() {
        assertEquals(4, calculadora.suma(2, 2), 0.001);
        assertEquals(10, calculadora.suma(5, 5), 0.001);
    }

    @Test
    public void resta() {
        assertEquals(15, calculadora.resta(25,10), 0.001);
        assertEquals(0, calculadora.resta(50,50), 0.001);
    }

    @Test
    public void multiplicacion() {
        assertEquals(12, calculadora.multiplicacion(2,6), 0.001);
        assertEquals(100, calculadora.multiplicacion(25,4), 0.001);
    }

    @Test
    public void division() {
        assertEquals(3, calculadora.division(6,2), 0.001);
        assertEquals(0, calculadora.division(100,0), 0.001);
    }
}