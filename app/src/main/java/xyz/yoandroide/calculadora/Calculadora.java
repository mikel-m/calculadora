package xyz.yoandroide.calculadora;

public class Calculadora {
    public double suma(int a, int b) {
        return (double) a + b;
    }

    public double resta(int a, int b) {
        return (double) a - b;
    }

    public double multiplicacion(int a, int b) {
        return (double) a * b;
    }

    public double division(int a, int b) {
        int respuesta = 0;
        if (b != 0) {
            respuesta = a / b;
        } else {
            return respuesta;
        }
        return respuesta;
    }
}
