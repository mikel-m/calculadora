#!/usr/bin/env bash

echo "Downloading google-services.json..."
echo $GOOGLE_SERVICES_JSON | base64 --decode > $APPCENTER_SOURCE_DIRECTORY/app/google-services.json